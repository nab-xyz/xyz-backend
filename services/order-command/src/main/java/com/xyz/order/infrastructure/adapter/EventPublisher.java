package com.xyz.order.infrastructure.adapter;

import com.xyz.order.infrastructure.repository.EventRepository;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableScheduling
public class EventPublisher {

	private final EventStreamConfig.OrderEventStream source;

	private final EventRepository eventRepository;

	public EventPublisher(
		EventStreamConfig.OrderEventStream source,
		EventRepository eventRepository
	) {
		this.source = source;
		this.eventRepository = eventRepository;
	}

	@Scheduled(fixedRate = 10000)
	@Transactional
	public void publishExternally() {
		eventRepository
			.findAllBySentIsFalseOrderByEventTimestamp()
			.forEach(event -> {
				boolean success = source
					.orderChannel()
					.send(
						MessageBuilder
							.withPayload(event.getContent())
							.setHeader("type", event.getSource())
							.build(), 3000
					);

				log.info("Event {} sent. success = {}", event.getSource(), success);
				if (success) {
					event.setSent(true);
					eventRepository.save(event);
				}
			});
	}
}
