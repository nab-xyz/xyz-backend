package com.xyz.order.infrastructure.repository;

import com.xyz.order.domain.entity.Event;
import java.util.stream.Stream;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {
  Stream<Event> findAllBySentIsFalseOrderByEventTimestamp();
}
