package com.xyz.order.application.command.orderpaid;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.application.command.base.Command;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;

public record OrderPaidCommand(
	@JsonProperty("orderId") @NotNull Long orderId,
	@JsonProperty("totalBeforeTax") BigDecimal totalBeforeTax,
	@JsonProperty("tax") BigDecimal tax

) implements Command {}
