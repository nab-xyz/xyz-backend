package com.xyz.order.application.controller;

import com.xyz.order.application.command.createorder.CreateOrderCommand;
import com.xyz.order.application.command.createorder.CreateOrderCommandHandler;
import com.xyz.order.application.command.createorder.CreateOrderCommandResult;
import com.xyz.order.application.command.orderpaid.OrderPaidCommand;
import com.xyz.order.application.command.orderpaid.OrderPaidCommandHandler;
import com.xyz.order.application.command.orderpaid.OrderPaidCommandResult;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrderCommandController {

	private final CreateOrderCommandHandler createOrderCommandHandler;

	private final OrderPaidCommandHandler orderPaidCommandHandler;

	public OrderCommandController(
		CreateOrderCommandHandler createOrderCommandHandler,
		OrderPaidCommandHandler orderPaidCommandHandler
	) {
		this.createOrderCommandHandler = createOrderCommandHandler;
		this.orderPaidCommandHandler = orderPaidCommandHandler;
	}

	@PostMapping("/orders")
	public ResponseEntity<CreateOrderCommandResult> createOrder(
		@RequestBody @Valid CreateOrderCommand command
	) {
		var result = createOrderCommandHandler.handle(command);
		return ResponseEntity.ok(result);
	}

	@PostMapping("/orders/{id}/paid")
	public ResponseEntity<OrderPaidCommandResult> payOrder(
		@RequestBody @Valid OrderPaidCommand command
	) {
		var result = orderPaidCommandHandler.handle(command);
		return ResponseEntity.ok(result);
	}
}
