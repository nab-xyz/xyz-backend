package com.xyz.order.application.command.createorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.application.command.base.Command;
import com.xyz.shared.valueobject.OrderItem;
import java.util.List;
import javax.validation.constraints.NotNull;

public record CreateOrderCommand(
	@JsonProperty("customerId") @NotNull String customerId,
	@JsonProperty("receiverName") String receiverName,
	@JsonProperty("deliveryAddress") String deliveryAddress,
	@JsonProperty("orderItems") @NotNull List<@NotNull OrderItem> orderItems

) implements Command {}
