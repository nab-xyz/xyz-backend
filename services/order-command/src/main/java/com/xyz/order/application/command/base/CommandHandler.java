package com.xyz.order.application.command.base;

public interface CommandHandler<C extends Command, R extends CommandResult> {
	R handle(C command);
}
