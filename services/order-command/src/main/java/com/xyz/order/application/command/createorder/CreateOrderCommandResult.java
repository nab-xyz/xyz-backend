package com.xyz.order.application.command.createorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.application.command.base.CommandResult;

public record CreateOrderCommandResult(
	@JsonProperty("success") boolean success,
	@JsonProperty("error") String error
) implements CommandResult {}
