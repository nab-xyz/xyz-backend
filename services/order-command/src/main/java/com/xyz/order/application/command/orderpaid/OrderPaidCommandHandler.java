package com.xyz.order.application.command.orderpaid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xyz.order.application.command.base.CommandHandler;
import com.xyz.order.domain.entity.Event;
import com.xyz.order.infrastructure.repository.EventRepository;
import com.xyz.shared.domainevent.OrderPaid;
import org.springframework.stereotype.Service;

@Service
public class OrderPaidCommandHandler
	implements CommandHandler<OrderPaidCommand, OrderPaidCommandResult> {

	private final EventRepository eventRepository;

	private final ObjectMapper objectMapper;

	public OrderPaidCommandHandler(
		EventRepository eventRepository,
		ObjectMapper objectMapper
	) {
		this.eventRepository = eventRepository;
		this.objectMapper = objectMapper;
	}

	@Override
	public OrderPaidCommandResult handle(OrderPaidCommand command) {
		try {
			var orderEvent = OrderPaid
				.builder()
				.orderId(command.orderId())
				.totalBeforeTax(command.totalBeforeTax())
				.tax(command.tax())
				.build();

			eventRepository.save(Event
				.builder()
				.source(orderEvent.type())
				.content(objectMapper.writeValueAsString(orderEvent))
				.build()
			);
		} catch (JsonProcessingException e) {
			return new OrderPaidCommandResult(false, e.getMessage());
		}
		return new OrderPaidCommandResult(true, null);
	}
}
