package com.xyz.order.application.command.orderpaid;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.application.command.base.CommandResult;

public record OrderPaidCommandResult(
	@JsonProperty("success") boolean success,
	@JsonProperty("error") String error
) implements CommandResult {}
