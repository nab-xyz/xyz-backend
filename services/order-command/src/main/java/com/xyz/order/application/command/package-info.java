/**
 * <pre>
 * - Command is an object that signals user intent, describes a single action
 * - used for state-changing actions
 * - should not return business data.
 * </pre>
 */
package com.xyz.order.application.command;