package com.xyz.order.application.command.createorder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xyz.order.application.command.base.CommandHandler;
import com.xyz.order.domain.entity.Event;
import com.xyz.order.infrastructure.repository.EventRepository;
import com.xyz.shared.domainevent.OrderCreated;
import org.springframework.stereotype.Service;

@Service
public class CreateOrderCommandHandler
	implements CommandHandler<CreateOrderCommand, CreateOrderCommandResult> {

	private final EventRepository eventRepository;

	private final ObjectMapper objectMapper;

	public CreateOrderCommandHandler(
		EventRepository eventRepository,
		ObjectMapper objectMapper
	) {
		this.eventRepository = eventRepository;
		this.objectMapper = objectMapper;
	}

	@Override
	public CreateOrderCommandResult handle(CreateOrderCommand command) {
		try {
			var orderEvent = OrderCreated
				.builder()
				.customerId(command.customerId())
				.receiverName(command.receiverName())
				.deliveryAddress(command.deliveryAddress())
				.orderItems(command.orderItems())
				.build();

			var content = objectMapper.writeValueAsString(orderEvent);

			eventRepository.save(Event
				.builder()
				.source(orderEvent.type())
				.content(content)
				.build()
			);
		} catch (JsonProcessingException e) {
			return new CreateOrderCommandResult(false, e.getMessage());
		}
		return new CreateOrderCommandResult(true, null);
	}
}
