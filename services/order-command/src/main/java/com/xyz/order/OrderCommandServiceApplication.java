package com.xyz.order;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan({"com.xyz.order", "com.xyz.shared"})
@EntityScan({"com.xyz.order"})
@EnableJpaRepositories({"com.xyz.order"})
public class OrderCommandServiceApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(OrderCommandServiceApplication.class)
			.properties("spring.config.additional-location:"
				+ "classpath:application-rest.yml,"
				+ "classpath:application-cloud-stream.yml")
			.build().run(args);
	}
}
