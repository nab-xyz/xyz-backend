//package order;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.xyz.order.OrderQueryServiceApplication;
//import com.xyz.shared.domainevent.OrderPlacedEvent;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest(classes = {OrderQueryServiceApplication.class},
//                webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@AutoConfigureMockMvc
//@AutoConfigureTestDatabase
//public class InvoiceApiIT {
//	@Autowired private MockMvc mockMvc;
//
//	@Autowired private ObjectMapper objectMapper;
//
//	@Test
//	public void testCreateOrder() throws Exception {
//		// given
//		OrderPlacedEvent validOrder = new OrderPlacedEvent("C1", "N1", "A1", 1L, 10);
//		OrderPlacedEvent orderWithInvalidProductId = new OrderPlacedEvent("C1", "N1", "A1", 2L, 10);
//		OrderPlacedEvent orderExceedAvailableQuantity = new OrderPlacedEvent("C1", "N1", "A1", 1L, 11);
//
//		// then
//		mockMvc
//			.perform(post("/orders")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsBytes(validOrder)))
//			.andDo(print())
//			.andExpect(status().is2xxSuccessful());
//
//		mockMvc
//			.perform(post("/orders")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsBytes(orderWithInvalidProductId)))
//			.andDo(print())
//			.andExpect(status().isBadRequest());
//
//		mockMvc
//			.perform(post("/orders")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsBytes(orderExceedAvailableQuantity)))
//			.andDo(print())
//			.andExpect(status().isBadRequest());
//	}
//}
