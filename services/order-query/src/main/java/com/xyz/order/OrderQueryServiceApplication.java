package com.xyz.order;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.xyz.order", "com.xyz.shared"})
@EntityScan({"com.xyz.order"})
@EnableJpaRepositories({"com.xyz.order"})
public class OrderQueryServiceApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(OrderQueryServiceApplication.class)
			.properties("spring.config.additional-location:"
				+ "classpath:application-rest.yml,"
				+ "classpath:application-cloud-stream.yml")
			.build().run(args);
	}
}
