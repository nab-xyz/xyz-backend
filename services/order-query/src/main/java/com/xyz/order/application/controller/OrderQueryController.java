package com.xyz.order.application.controller;

import com.xyz.order.application.query.order.OrderQuery;
import com.xyz.order.application.query.order.OrderQueryHandler;
import com.xyz.order.application.query.order.OrderQueryResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrderQueryController {

	private final OrderQueryHandler orderQueryHandler;

	public OrderQueryController(OrderQueryHandler orderQueryHandler) {
		this.orderQueryHandler = orderQueryHandler;
	}

	@GetMapping("/orders")
	public ResponseEntity<OrderQueryResult> getOrders(@RequestBody OrderQuery query) {
		var result = orderQueryHandler.handle(query);
		return ResponseEntity.ok(result);
	}
}
