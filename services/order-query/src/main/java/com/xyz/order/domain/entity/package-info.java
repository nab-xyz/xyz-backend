/**
 * <pre>
 *   Entities:
 *   - encapsulate Enterprise-wide business rules and attributes
 *   - represent business models and express what properties a particular model has, what it can do,
 *      when and at what conditions it can do it
 * </pre>
 */
package com.xyz.order.domain.entity;