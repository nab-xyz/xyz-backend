/**
 * <pre>
 * - Query intent to find something and describes how to do it.
 * - Query is just a data retrieval operation and should not make any state changes
 * </pre>
 */
package com.xyz.order.application.query;