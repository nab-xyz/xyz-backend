package com.xyz.order.application.query.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.domain.entity.aggregate.Order;
import com.xyz.order.application.query.base.QueryResult;
import java.util.List;

public record OrderQueryResult(
	@JsonProperty("orders") List<Order> orders
) implements QueryResult {
}
