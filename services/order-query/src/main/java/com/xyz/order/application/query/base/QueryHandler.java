package com.xyz.order.application.query.base;

public interface QueryHandler<Q extends Query, R extends QueryResult> {
	R handle(Q query);
}