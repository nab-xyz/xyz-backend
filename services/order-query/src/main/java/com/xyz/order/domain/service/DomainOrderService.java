package com.xyz.order.domain.service;

import com.xyz.order.domain.entity.Invoice;
import com.xyz.order.domain.entity.aggregate.Order;
import com.xyz.order.domain.entity.OrderLineItem;
import com.xyz.order.infrastructure.adapter.ProductService;
import com.xyz.order.infrastructure.repository.InvoiceRepository;
import com.xyz.order.infrastructure.repository.OrderRepository;
import com.xyz.shared.valueobject.OrderItem;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class DomainOrderService {
	private final ProductService productService;

	private final OrderRepository orderRepository;

	private final InvoiceRepository invoiceRepository;

	public DomainOrderService(
		ProductService productService,
		OrderRepository orderRepository,
		InvoiceRepository invoiceRepository
	) {
		this.productService = productService;
		this.orderRepository = orderRepository;
		this.invoiceRepository = invoiceRepository;
	}

	public void createOrder(
		@NonNull String customerId,
		String receiverName,
		String deliveryAddress,
		@NonNull List<@NonNull OrderItem> orderItems
	) {
		var order = Order
			.builder()
			.customerId(customerId)
			.receiverName(receiverName)
			.deliveryAddress(deliveryAddress)
			.orderLineItems(
				orderItems
					.stream()
					.map(item -> OrderLineItem
						.builder()
						.productId(item.productId())
						.quantity(item.quantity())
						.price(productService.getProductPrice(item.productId()))
						.build()
					)
					.collect(Collectors.toSet())
			)
			.build();

		orderRepository.save(order);
	}

	public void updatePaidOrder(Long orderId, BigDecimal totalBeforeTax, BigDecimal tax) {
		var orderOptional = orderRepository.findById(orderId);
		if (orderOptional.isEmpty()) return;

		var order = orderOptional.get();
		order.setStatus(Order.OrderStatus.PAID);
		var invoice = Invoice
			.builder()
			.orderId(order.getId())
			.totalBeforeTax(totalBeforeTax)
			.tax(tax)
			.build();
		invoiceRepository.save(invoice);
	}
}
