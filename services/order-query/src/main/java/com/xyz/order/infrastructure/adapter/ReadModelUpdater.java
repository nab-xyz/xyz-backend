package com.xyz.order.infrastructure.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xyz.order.domain.service.DomainOrderService;
import com.xyz.shared.domainevent.OrderCreated;
import com.xyz.shared.domainevent.OrderPaid;
import java.io.IOException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReadModelUpdater {

	private final DomainOrderService orderService;

	private final ObjectMapper objectMapper;

	public ReadModelUpdater(DomainOrderService orderService, ObjectMapper objectMapper) {
		this.orderService = orderService;
		this.objectMapper = objectMapper;
	}

	@StreamListener("order-channel")
	public void handleEvents(@Payload byte[] ev, @Header String type) throws IOException {
		switch (type) {
			case OrderCreated.TYPE ->
				handleOrderCreatedEvent(objectMapper.readValue(ev, OrderCreated.class));
			case OrderPaid.TYPE -> handleOrderPaidEvent(objectMapper.readValue(ev, (OrderPaid.class)));
			default -> {}
		}
	}

	@Transactional
	private void handleOrderCreatedEvent(OrderCreated event) {
		orderService.createOrder(
			event.customerId(),
			event.receiverName(),
			event.deliveryAddress(),
			event.orderItems()
		);
	}

	@Transactional
	private void handleOrderPaidEvent(OrderPaid event) {
		orderService.updatePaidOrder(event.orderId(), event.totalBeforeTax(), event.tax());
	}
}
