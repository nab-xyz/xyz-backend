/**
 * - Repositories centralize common data access functionality and encapsulate the logic required to
 * access that data
 */
package com.xyz.order.infrastructure.repository;