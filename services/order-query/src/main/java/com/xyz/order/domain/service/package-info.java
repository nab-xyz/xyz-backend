/**
 * <pre>
 *  - Domain services are used for "a significant process or transformation in the domain that is
 *    not a natural responsibility of an ENTITY or VALUE OBJECT"
 *  - Domain Services are used when putting the logic on a particular Entity would break encapsulation
 *    and require the Entity to know about things it really shouldn't be concerned with.
 * </pre>
 */
package com.xyz.order.domain.service;