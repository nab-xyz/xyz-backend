package com.xyz.order.application.query.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.order.application.query.base.Query;

public record OrderQuery(
	@JsonProperty("customerId") String customerId
) implements Query {
}
