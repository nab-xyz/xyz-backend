package com.xyz.order.domain.entity.aggregate;

import com.xyz.order.domain.entity.Invoice;
import com.xyz.order.domain.entity.OrderLineItem;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * aggregate root
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "`order`", schema = "PUBLIC")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String customerId;

	private String receiverName;

	private String deliveryAddress;

	@Builder.Default
	@Enumerated(EnumType.STRING)
	private OrderStatus status = OrderStatus.DRAFT;

	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "order_id")
	private Invoice invoice;

	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "order")
	private Set<OrderLineItem> orderLineItems;

	public static enum OrderStatus {DRAFT, PAID}
}
