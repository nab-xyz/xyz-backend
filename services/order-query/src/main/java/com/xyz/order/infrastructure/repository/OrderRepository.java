package com.xyz.order.infrastructure.repository;

import com.xyz.order.domain.entity.aggregate.Order;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
	List<Order> findAllByCustomerId(String customerId);
}
