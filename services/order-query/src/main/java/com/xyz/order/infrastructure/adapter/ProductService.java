package com.xyz.order.infrastructure.adapter;

import java.math.BigDecimal;
import java.util.random.RandomGenerator;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
	/**
	 * Fake service for generating random value as product's price. In real life this would be API
	 * call to inventory service which keeps product's info
	 */
	public BigDecimal getProductPrice(String productId) {
		return BigDecimal.valueOf(RandomGenerator.getDefault().nextDouble());
	}
}
