package com.xyz.order.application.query.order;

import com.xyz.order.application.query.base.QueryHandler;
import com.xyz.order.infrastructure.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderQueryHandler implements QueryHandler<OrderQuery, OrderQueryResult> {

	private final OrderRepository orderRepository;

	public OrderQueryHandler(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	@Override
	public OrderQueryResult handle(OrderQuery query) {
		return new OrderQueryResult(orderRepository.findAllByCustomerId(query.customerId()));
	}
}
