/**
 * <pre>
 * - Infrastructure adapters (also called driven/secondary adapters)
 *   enable a software system to interact with external systems by receiving, storing and providing
 *   data when requested (like persistence, message brokers, sending emails or messages, requesting
 *   3rd party APIs etc).
 * </pre>
 */
package com.xyz.order.infrastructure.adapter;