/**
 * <pre>
 *   - Aggregate is a cluster of domain objects that can be treated as a single unit.
 *
 * </pre>
 */
package com.xyz.order.domain.entity.aggregate;