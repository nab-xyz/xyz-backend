package com.xyz.shared.jpa.config;

import com.xyz.shared.exception.XyzApiException;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private MessageSourceAccessor messageSourceAccessor;

	public GlobalExceptionHandler(MessageSource messageSource) {
		this.messageSourceAccessor = new MessageSourceAccessor(messageSource);
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor stringTrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringTrimmer);
	}

	@ExceptionHandler(XyzApiException.class)
	private ResponseEntity<Object> handleApiException(
		XyzApiException ex,
		WebRequest request
	) {
		return handleExceptionInternal(
			ex,
			ex.message,
			null,
			HttpStatus.BAD_REQUEST,
			request
		);
	}

	@ExceptionHandler(HttpClientErrorException.class)
	private ResponseEntity<Object> handleHttpClientErrorException(
		HttpClientErrorException ex,
		WebRequest request
	) {
		return handleExceptionInternal(
			ex,
			ex.getResponseBodyAsString(),
			ex.getResponseHeaders(),
			ex.getStatusCode(),
			request
		);
	}
}
