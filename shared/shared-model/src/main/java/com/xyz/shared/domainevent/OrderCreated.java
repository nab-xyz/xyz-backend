package com.xyz.shared.domainevent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.xyz.shared.valueobject.OrderItem;
import java.util.List;
import lombok.Builder;
import lombok.NonNull;

@Builder
public record OrderCreated(
	@JsonProperty("customerId") @NonNull String customerId,
	@JsonProperty("receiverName") String receiverName,
	@JsonProperty("deliveryAddress") String deliveryAddress,
	@JsonProperty("orderItems") @NonNull List<@NonNull OrderItem> orderItems
) implements DomainEvent {
	public static final String TYPE = "ORDER_CREATED";

	@Override public String type() {
		return TYPE;
	}
}
