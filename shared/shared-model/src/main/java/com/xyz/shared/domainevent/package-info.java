/**
 * <pre>
 *   - Domain Event indicates that something happened in a domain that you want other parts of the
 *   same domain (in-process) to be aware of
 * </pre>
 */
package com.xyz.shared.domainevent;