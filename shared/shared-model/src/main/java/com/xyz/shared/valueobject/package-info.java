/**
 * <pre>
 *   Value Objects:
 *   - Have no identity. Equality is determined through structural property.
 *   - immutable
 *   - Explicitly defines and enforces important constraints
 * </pre>
 */
package com.xyz.shared.valueobject;