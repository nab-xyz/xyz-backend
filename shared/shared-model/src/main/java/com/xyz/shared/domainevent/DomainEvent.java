package com.xyz.shared.domainevent;

public interface DomainEvent {
  String type();
}
