package com.xyz.shared.valueobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

public record OrderItem(
	@JsonProperty("productId") @NonNull String productId,
	@JsonProperty("quantity") @NonNull Integer quantity
) {}
