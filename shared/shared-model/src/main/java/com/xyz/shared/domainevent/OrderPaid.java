package com.xyz.shared.domainevent;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.NonNull;

@Builder
public record OrderPaid(
	@JsonProperty("orderId") @NonNull Long orderId,
	@JsonProperty("totalBeforeTax") @NonNull BigDecimal totalBeforeTax,
	@JsonProperty("tax") @NonNull BigDecimal tax
) implements DomainEvent {

	public static final String TYPE = "ORDER_PAID";

	@Override public String type() {
		return TYPE;
	}
}
