package com.xyz.shared.exception;

public class XyzApiException extends RuntimeException {
  public String message;

  public XyzApiException(String message) {
    super(message);
    this.message = message;
  }
}
