## High-level solution diagram
- This project demonstrates of use microservice development patterns using an example of a very simple online shopping application
![High-level solution diagram](images/high-level.png "High-level solution diagram")
- This application contains 2 services:
  + `order-query` service
  + `order-command` service
- Inter-services communication can be synchronous by directly calling RESTful APIs (with JSON payload) or asynchronous using message broker (rabbitmq)

## Project layout
- The project format is multi-modules Gradle project
- There are 2 types of modules:
### Service modules 
- Contain business logic code and will be built to docker images and deployed as a service in microservice system. 
- e.g.
  + `order-query`
  + `order-command`
### Shared modules: 
- Contain some shared codes, configs, libs, ... The modules produced by this code are intended to be consumed by service modules. 
- Has prefix `shared-` in their name
- e.g
  + `shared-model`: Contains shared DTOs, constants, domain events, value objects
  + `shared-rest`: Contains common `spring data REST` configs for exposing APIs endpoints
  + `shared-cloud-stream`: Contains common `spring cloud stream` & `rabbitmq` configs in order to produce/consume message via message broker

## Building the code and run application locally
### Prerequisites
- The project is using Java 17
- Gradle is used as the project definition format. Gradle version >=5 should be used
- A Docker image build environment. To create and build a microservice Docker image, Docker must be available.

### Building the code
- With Java and Gradle installed, building the application follows the typical Gradle build lifecycle
- This repo has contained some pre-defined build scripts:
  + `build-project.sh`: build project
  + `build-docker-images.sh`: build project & docker images using google jib
- The service modules has been configured with [gradle-docker-plugin](https://bmuschko.github.io/gradle-docker-plugin/#spring_boot_application_plugin) so the results will be a docker image
- After cloning this repo to your machine, `cd` to it run following scrip to build docker images:
```shell script
./build-docker-images.sh
```

### Deploy local
- Build the project
- Create a docker swarm
```shell script
docker swarm init --advertise-addr=127.0.0.1
```
- Deploy docker stack
```shell script
docker stack deploy -c docker/docker-stack.local.yml --prune --resolve-image always xyz
```

- `order-query` service will run on port 8080, `order-command` service will run on port 8081 

## APIs specification & Swagger UI
- `order-query`: localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/
- `order-command`: localhost:8081/swagger-ui/index.html?configUrl=/v3/api-docs/